# Waracle iOS Tech Test Notes

## To be completed by the candidate

Name: _______

Version of Xcode used: _______

Amount of time taken to complete test: _______

### Any suggestions of things you would add, improve or do differently with more time
- ...
- ...


### Any other comments or information about your solution
- ...
- ...
